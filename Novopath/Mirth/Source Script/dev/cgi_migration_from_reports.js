	var tigerDbConnSource;
	var tigerDbConnDest;
	var oldPracticeZip = 0;
	var count = 10;
	var defaultPhysFName = 'Novopath';
	var defaultPhysLName = 'Default';

	var tiger_dbUrlSource = "jdbc:mysql://10.10.10.10/MIGRATE_CGI_DATA";
	var tiger_sourcedatabaseUsername = "root";
	var tiger_sourcedatabasePassword = "P@rad!gm";
	
	var tiger_dbUrlDest = "jdbc:mysql://10.10.10.10/Tiger_11072019_uat";
	var tiger_DestdatabaseUsername = "root";
	var tiger_DestdatabasePassword = "P@rad!gm";

	var npId;
	var errorCounter = 0;
	var rowsInserted = 0;

	try
	{
		tigerDbConnSource = DatabaseConnectionFactory.createDatabaseConnection('com.mysql.jdbc.Driver', 
			tiger_dbUrlSource,tiger_sourcedatabaseUsername, tiger_sourcedatabasePassword);
		tigerDbConnDest = DatabaseConnectionFactory.createDatabaseConnection('com.mysql.jdbc.Driver',tiger_dbUrlDest,
			tiger_DestdatabaseUsername, tiger_DestdatabasePassword);

		var defaultPhysicianResult = tigerDbConnDest.executeCachedQuery("SELECT PHYSICIAN_ID FROM `PHYSICIAN` " + 
			"WHERE PHYSICIAN_FIRST_NAME = '" + defaultPhysFName + "' AND PHYSICIAN_LAST_NAME = '" + defaultPhysLName + "'");
		if(defaultPhysicianResult == null || defaultPhysicianResult.size() == 0){
			tigerDbConnDest.executeUpdate("INSERT INTO PHYSICIAN (PHYSICIAN_FIRST_NAME, PHYSICIAN_LAST_NAME, " + 
				"CREATED_BY, CREATION_DATE)" + 
				"VALUES ('" + defaultPhysFName + "', '" + defaultPhysLName + "', 'System', NOW())");
		}
		
		try{

			tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 "`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " + 
			 "`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
			 "VALUES ('116', 'CGI-cytogenetics-novopath', 'CACG', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
			 	"VALUES ('117', 'CGI-FLOW-novopath', 'CAFC', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('118', 'CGI-FISH-novopath', 'CAFH', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('119', 'CGI-FHACT-novopath', 'CAHT', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('120', 'CGI-molecular-novopath', 'CAM', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('121', 'CGI-research-novopath', 'CARM', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('122', 'CGI-surgical-novopath', 'CAS', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('123', 'CGI-Urovysion-novopath', 'CAUV', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
			 tigerDbConnDest.executeUpdate("INSERT INTO `CASE_CATEGORY` (`CASE_CATEGORY_ID`, `CASE_CATEGORY_NAME`, `CASE_CATEGORY_PREFIX`, " +
			 	"`CASE_CATEGORY_INITIAL_SEED`, `CASE_CATEGORY_CURRENT_SEED`, `CASE_CATEGORY_AUTO_GENERATED`, `LAST_UPDATE_DATE`, " +
			 	"`ENABLE_NOTIFICATION_SERVICE`, `CREATED_BY`, `CREATION_DATE`, `CASE_CATEGORY_ACTIVE`) " +
				"VALUES ('124', 'CGI-summation-novopath', 'CAX', '1', '1', '1', NOW(), '0', 'System', NOW(), '0');");
		}catch(err){
			logger.error("Error ocurred removing/adding categories: " + err.message);
		}
		
		var result = tigerDbConnSource.executeCachedQuery("SELECT "
						+"novopath.novo_table_id, "
						+"novopath.Patient_First_Name, "
						+"novopath.Patient_Last_Name, "
						+"novopath.Patient_Date_of_Birth, "
						+"novopath.Physician_Name, "
						+"novopath.Practice_Name, "
						+"novopath.Practice_Address, "
						+"novopath.Practice_City, "
						+"novopath.Practice_State, "
						+"novopath.Practice_Zip, "
						+"novopath.Practice_Phone, "
						+"novopath.Case_Number, "
						+"novopath.Date_Received, "
						+"novopath.Date_Collected, "
						+"novopath.Date_Reported, "
						+"novopath.Sample_Type, "
						+"novopath.Clinical_History, "
						+"novopath.Report_Name "
						+"FROM MIGRATE_CGI_DATA.novopath_from_reports novopath "
						+"WHERE Practice_State in ('AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'HI', "
						+"'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MS', 'MT', "
						+"'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', "
						+"'TN', 'TX', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY', 'PR', 'CO', 'VI', 'BS', 'MX') "
						+"AND Practice_Zip <> '00000' AND Practice_Zip is NOT null AND Practice_Name is NOT NULL "
						+"AND Patient_First_Name is NOT null AND Patient_Last_Name is NOT null AND Date_Reported IS NOT NULL "
						+"AND Date_Reported <> 'NA'"
						+"GROUP BY Case_Number ORDER BY Practice_Zip;");
						
		var errorCounter = 0;
		var rowsInserted = 0;

		while (result.next()){
			try{
				npId = result.getInt('novo_table_id');
				
				var caseNumber = result.getString('Report_Name');
				var caseNumberArr = caseNumber.split("\\.");
				var caseCode = caseNumberArr[0];
				logger.info("Case code: " + caseCode);

				var caseExistsBefore = tigerDbConnDest.executeCachedQuery("SELECT CASE_CODE FROM `CASE` WHERE CASE_CODE = '" + 
					caseCode + "'");
				//logger.info("SQL to run is (caseExistsBefore): " + "SELECT CASE_CODE FROM `CASE` WHERE CASE_CODE = '" + 
				//	caseCode + "'");
				if (caseExistsBefore != null && caseExistsBefore.size() > 0){
					logger.info("Case inserted before, aborting the current iteration, case code: " + caseCode);
					continue;
				}


				//=== Physician info === 
				var physicianName = result.getString('Physician_Name');
				var physLName, physFName = null;
				var physLNameMd, physLNamePhd, physLNameNoMd, physLNameNoPhd, physLNameNoMdNoPhd;
				if(physicianName != null && (physicianName.split(" ").length > 2 
					|| (physicianName.split(" ").length == 2 && physicianName.split(" ")[1] != "MD" 
						&& physicianName.split(" ")[1] != "PhD" ))) {
					physicianName = new String(physicianName);
					physicianName = physicianName.replace(/\'/g, "\\'");

					var indexOfDot = physicianName.indexOf(".");
					//logger.info("Dot found at index: " + indexOfDot + ", while physicianName is: " + physicianName);
					if(indexOfDot > -1 && indexOfDot != physicianName.length - 1){
						physFName = physicianName.substring(0, indexOfDot) + "";
						physLName = physicianName.substring(indexOfDot + 1, physicianName.length).trim() + "";
						//logger.info("dot Physician first name: " + physFName + ", and last name: " + physLName);
					}
					else{
						var indexOfSpace = physicianName.indexOf(" ");
						physFName = physicianName.substring(0, indexOfSpace) + "";
						physLName = physicianName.substring(indexOfSpace + 1, physicianName.length) + "";
						//logger.info("space Physician first name: " + physFName + ", and last name: " + physLName);
					}
					
					physLNameMd = physLName + ", MD";
					physLNamePhd = physLName + ", PhD";
					physLNameNoMd = physLName.replace(", MD", "");
					physLNameNoPhd = physLName.replace(", PhD", "");
					physLNameNoMdNoPhd = physLName.replace(", PhD", "").replace(", MD", "");

				}else{
					physFName = defaultPhysFName;
					physLName = defaultPhysLName;
					physLNameMd = defaultPhysLName;
					physLNamePhd = defaultPhysLName;
					physLNameNoMd = defaultPhysLName;
					physLNameNoPhd = defaultPhysLName;
					physLNameNoMdNoPhd = defaultPhysLName;
				}

				var physicianId;
				var physicianResult = tigerDbConnDest.executeCachedQuery("SELECT PHYSICIAN_ID FROM PHYSICIAN " + 
					"WHERE PHYSICIAN_FIRST_NAME ='" + physFName + "' " +
					"AND (PHYSICIAN_LAST_NAME = '" + physLName + "' OR PHYSICIAN_LAST_NAME = '" + physLNameMd + "' " +
					"OR PHYSICIAN_LAST_NAME = '" + physLNamePhd + "' OR PHYSICIAN_LAST_NAME = '" + physLNameNoMd + "' " +
					"OR PHYSICIAN_LAST_NAME = '" + physLNameNoPhd + "' OR PHYSICIAN_LAST_NAME = '" + physLNameNoMdNoPhd + "')");
				
				//logger.info("Physician first name: " + physFName + ", last name: " + physLName);
				if (physicianResult.size() > 0){
					//logger.info("Will not enter physician data, as it already exists");
					while(physicianResult.next()){
						physicianId = physicianResult.getInt(1);
						//logger.info("Get physicianId: " + physicianId);
					}					
				}else{
					//logger.info("About to insert new physician!");
					var insertPhysician = "INSERT INTO `PHYSICIAN`"+
										"(`PHYSICIAN_FIRST_NAME`,"+
										"`PHYSICIAN_LAST_NAME`,"+
										"`CREATED_BY`,"+
										"`CREATION_DATE`)"+
										"VALUES"+
										"('"+ physFName +"', '" + physLName + "', 'System', NOW())";
					//logger.info("Insert Physicians: " + insertPhysician);
					tigerDbConnDest.executeUpdate(insertPhysician);
					
					physicianResult = tigerDbConnDest.executeCachedQuery("SELECT MAX(PHYSICIAN_ID) FROM PHYSICIAN");
					while(physicianResult.next()){
						physicianId = physicianResult.getInt(1);
					}
				}

				//=== Client info ===
				var practiceZip = result.getString('Practice_Zip');
				if(practiceZip != null){
					practiceZip = ""+ practiceZip;
					//logger.info("Practice zip code is: " + practiceZip + ", and its length is: " + practiceZip.length);
				}
				
				while(practiceZip != null && practiceZip.length < 5){
					practiceZip = "0" + practiceZip;
				}
				var practiceCode = practiceZip;
				if(oldPracticeZip == practiceZip){
					practiceCode = practiceZip + "-" + count;
					count++;
				}else{
					count = 10;
				}

				oldPracticeZip = practiceZip;
				//logger.info("PracticeCode after: " + practiceCode);				
				
				var practiceName = result.getString('Practice_Name');
				if(practiceName != null){
					practiceName = new String(practiceName);
					practiceName = practiceName.replace(/\'/g, "\\'");
				}
				
				var practiceAddress = result.getString('Practice_Address');
				var practiceCity = result.getString('Practice_City');
				
				var clientId = 0;
				var oldClient = false;
				//logger.info("Before get client info practice name: " + practiceName);
				var clientResult = tigerDbConnDest.executeCachedQuery("SELECT cl.CLIENT_ID FROM CLIENT cl " + 
					"INNER JOIN CLIENT_ADDRESS clad ON cl.CLIENT_ID = clad.CLIENT_ID " +
					"WHERE cl.CLIENT_NAME = '" + practiceName +"' AND clad.ZIP_CODE = '" + practiceZip + "' ");
				if (clientResult.size() > 0){
					while(clientResult.next()){
						clientId = clientResult.getInt(1);
						oldClient = true;
					}
				}
				
				//logger.info("After getting client id: " + clientId);
				if(!oldClient){
					var pCodeReserved = 1;
					while(pCodeReserved == 1){
						//logger.info("Inside the while loop, trying to get unused practice code: " + practiceCode)
						pCodeReserved = 0;
						var practiceCodeReserved = tigerDbConnDest.executeCachedQuery("SELECT PRACTICE_CODE FROM CLIENT " + 
							"WHERE PRACTICE_CODE = '" + practiceCode +"' ");
						if(practiceCodeReserved.size() > 0){
							while(practiceCodeReserved.next()){
								pCodeReserved = 1;
								practiceCode = practiceZip + "-" + count;
								count++;
							}
						}
					}
					
					//logger.info("Before inserting client info");
					var insertClient = "INSERT INTO `CLIENT`"
									+"(`CLIENT_NAME`,"
									+"`PRACTICE_CODE`,"
									+"`ACCOUNT_TYPE_ID`,"
									+"`CLIENT_PERIORITY_ID`,"
									+"`CLIENT_REPORT_BY_EMAIL`,"
									+"`CLIENT_WORD_REPORT_IN_WEB`,"
									+"`CLIENT_DO_NOT_RETURN`,"
									+"`CREATED_BY`,"
									+"`CREATION_DATE`)"
									+"VALUES"
									+"('"+ practiceName + "', '" + practiceCode + "', 1, 6, 1, 0, 0, 'System', NOW())";
					tigerDbConnDest.executeUpdate(insertClient);
					//logger.info("After inserting client info");
					var _clientResult = tigerDbConnDest.executeCachedQuery
						("SELECT CLIENT_ID FROM CLIENT WHERE CLIENT_NAME = '" + practiceName + "'");
					while(_clientResult.next()){
						clientId = _clientResult.getInt('CLIENT_ID');
					}
				}

				if(!oldClient){ //i.e. new client insert his/her address
					//Get the state of client
					var stateId = null;
					var practiceState = result.getString('Practice_State');
					if(practiceState != null){
						var stateResult = tigerDbConnDest.executeCachedQuery("SELECT STATE_ID FROM STATE " + 
							"WHERE STATE_SHORT_NAME = '" + practiceState + "' AND STATE_COUNTRY_ID=1");
						//logger.info("Get the state for client: " + practiceState);
						if (stateResult.size() > 0){
							while(stateResult.next()){
							stateId = stateResult.getInt(1);
							}
						}
						//logger.info("After getting stateId: " + stateId);
					}

					//=== Client address ===
					var inserClientAddress = "INSERT INTO `CLIENT_ADDRESS`"
											+"(`ADDRESS_LINE_1`,"
											+"`CITY_NAME`,"
											+"`ZIP_CODE`,"
											+"`CLIENT_ID`,"
											+"`ADDRESS_TYPE_ID`,"
											+"`STATE_ID`,"
											+"`CREATED_BY`,"
											+"`CREATION_DATE`)"
											+"VALUES"				//[H.Ibraheem] Test that clients are linked with addresses
											+"(" + (practiceAddress != null? "'" + practiceAddress + "'" : null) + ", "
											+(practiceCity != null? "'" + practiceCity  + "'" : null) + ", "
											+(practiceZip != null? "'" + practiceZip  + "'" : null) + "," + clientId 
											+",1," + stateId + ",'System',NOW())";
					
					//logger.info("Insertion SQL (inserClientAddress): " + inserClientAddress);
					tigerDbConnDest.executeUpdate(inserClientAddress);
					var maxClientAddressId = tigerDbConnDest.executeCachedQuery("SELECT MAX(CLIENT_ADDRESS_ID) FROM CLIENT_ADDRESS");
					while(maxClientAddressId.next()){
						clientAddressId = maxClientAddressId.getInt(1);
					}
					tigerDbConnDest.executeUpdate("UPDATE `CLIENT` SET CLIENT_DEFAULT_ADDRESS = " + clientAddressId +
						" WHERE CLIENT_ID = " + clientId);
				}

				var clientPhone = result.getString('Practice_Phone');
				if(clientPhone != null){
					var clientsPhoneExists = tigerDbConnDest.executeCachedQuery("SELECT CONTACT_VALUE FROM CLIENT_CONTACT_TYPE " +
						"WHERE CLIENT_ID = " + clientId + " AND CONTACT_VALUE = '" + clientPhone + "'");
					var clientContactExists = false;
					while(clientsPhoneExists.next()){
						clientContactExists = true;
					}
					if(!clientContactExists){
						var insertClientContact = tigerDbConnDest.executeUpdate("INSERT INTO CLIENT_CONTACT_TYPE(CLIENT_ID, " + 
						"CONTACT_TYPE_ID, CONTACT_VALUE, CREATED_BY, CREATION_DATE) " + 
						"VALUES(" + clientId + ", 2, '" + clientPhone + "', 'System', NOW())");
					}
					
				}


				//=== Client Physician ===
				//logger.info("Adding a relation between this client: " + clientId + ", and that physician: " + physicianId);
				var clientPhysicianRelResult = tigerDbConnDest.executeCachedQuery("SELECT CLIENT_PHYSICIAN_ID FROM CLIENT_PHYSICIAN " + 
					"WHERE CLIENT_ID = '" + clientId + "' AND PHYSICIAN_ID = '" + physicianId + "'");
				if(clientPhysicianRelResult == null || clientPhysicianRelResult.size() == 0){
					var clientPhysicianRelInsert = tigerDbConnDest.executeUpdate("INSERT INTO CLIENT_PHYSICIAN (" 
						+ "CLIENT_ID, PHYSICIAN_ID, IS_ATTENDING_PHYSICIAN, CREATED_BY, CREATION_DATE) " 
						+ "VALUES(" + clientId + ", " + physicianId + ", 1, 'System', NOW())");
				}


				//=== Patient info ===
				var patientId = 1;
				var patientFirstName = result.getString('Patient_First_Name');
				patientFirstName = new String(patientFirstName);
				patientFirstName= patientFirstName.replace(/\'/g, "\\'");
				
				var patientLastName = result.getString('Patient_Last_Name');
				patientLastName = new String(patientLastName);
				patientLastName= patientLastName.replace(/\'/g, "\\'");
				
				var patientMiddleName = patientFirstName.match(/\s[a-zA-Z]{1}\.$/) + "";
				var patientDateofBirth = result.getString('Patient_Date_of_Birth');

				if(patientMiddleName != null && patientMiddleName != ""){
					patientFirstName = patientFirstName.replace(patientMiddleName, "");
					patientMiddleName = patientMiddleName.substring(1, 2);
				}

				if(patientDateofBirth != null && patientDateofBirth != 'NA' && patientDateofBirth != "---" 
					&& patientDateofBirth != "--"){
					patientDateofBirth = patientDateofBirth + "";
					logger.info("Patient date of birth: " + patientDateofBirth);
					patientDateofBirth = DateUtil.getDate("MM/dd/yyyy", 
						patientDateofBirth.substring(0, Math.min(patientDateofBirth.length, 10)));
				}else{
					patientDateofBirth = DateUtil.getDate("MM/dd/yyyy", "01/01/1900");
				}
				logger.info("Patient date of birth: "+ patientDateofBirth);
				patientDateofBirth = DateUtil.formatDate("yyyy-MM-dd", patientDateofBirth);

				var patientResult = tigerDbConnDest.executeCachedQuery
					("SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_FIRST_NAME = '" + patientFirstName 
					+ "' AND PATIENT_LAST_NAME = '" + patientLastName + "' AND DATE_OF_BIRTH = '" + patientDateofBirth + "'");

				logger.info("Patient first name: " + patientFirstName + ", last name: " + patientLastName + 
					", date of birth: " + patientDateofBirth);

				var patientFound = false;
				while(patientResult.next()){
					patientId = patientResult.getInt(1);
					patientFound = true;
				}
				
				//logger.info("Patient found: " + patientFound + ", while patient id: " + patientId + 
				//	", and middle name: " + patientMiddleName);
				
				if(!patientFound){
					var insertPatient =  "INSERT INTO `PATIENT`"
										+"(`PATIENT_SSN`,"
										+"`PATIENT_FIRST_NAME`,"
										+"`PATIENT_MIDDLE_INITIAL`,"
										+"`PATIENT_LAST_NAME`,"
										+"`DATE_OF_BIRTH`,"
										+"`MIGRATION_ID`,"
										+"`CREATED_BY`,"
										+"`CREATION_DATE`)"
										+"VALUES"
										+"('', '" + patientFirstName + "', " 
										+ (patientMiddleName != null? "'" + patientMiddleName + "'" : null) 
										+ ", '" + patientLastName + "', '" + patientDateofBirth + "', " + npId + ", 'System', NOW())";
					//logger.info("Patient insertion SQL(insertPatient): " + insertPatient);
					tigerDbConnDest.executeUpdate(insertPatient);
					//get patient id
					var pateintIdResult = tigerDbConnDest.executeCachedQuery("SELECT MAX(PATIENT_ID) FROM PATIENT");
					
					if(pateintIdResult.size() > 0){
						while(pateintIdResult.next()){
							patientId = pateintIdResult.getInt(1);
							
						}
					}
				}else{
					var updatePatient =  "UPDATE `PATIENT` SET PATIENT_HISTORY = 1 WHERE PATIENT_ID = " + patientId;
					//logger.info("Patient UPDATE SQL(updatePatient): " + updatePatient);
					tigerDbConnDest.executeUpdate(updatePatient);
				}
				
				var caseCategoryPrefix =  caseCode.split("-")[0] + "";
				//logger.info("Case category prefix: " + caseCategoryPrefix);
				caseCategoryPrefix = caseCategoryPrefix.substring(0, caseCategoryPrefix.length - 2);
				
				//logger.info("caseCategoryPrefix: " + caseCategoryPrefix);
				var caseCategoryIdResult = tigerDbConnDest.executeCachedQuery("SELECT CASE_CATEGORY_ID FROM CASE_CATEGORY " +
					"WHERE `CASE_CATEGORY_NAME` LIKE '%novopath%' AND  `CASE_CATEGORY_PREFIX` = '" + caseCategoryPrefix + "'");
				
				var caseCategoryId;
				while(caseCategoryIdResult.next()){
					caseCategoryId = caseCategoryIdResult.getInt(1);
				}
				
				logger.info("Case code: " + caseCode + ", case category id: " + caseCategoryId + ", client id: " + clientId);
				
				var dateReceived = result.getString('Date_Received');
				var dateCollected = result.getString('Date_Collected');	
				var dateReported = result.getString('Date_Reported') + "";
				if(dateReceived != null && dateReceived != "NA" && dateReceived != "Not provided" 
					&& dateReceived != "---" && dateReceived != "N/D" && dateReceived != "Date is not provided"
					&& dateReceived != "--"){
					//logger.info("Received date: " + dateReceived);
					dateReceived = dateReceived + "";
					dateReceived = DateUtil.getDate("MM/dd/yyyy", dateReceived.substring(0, Math.min(dateReceived.length, 10)));
					dateReceived = DateUtil.formatDate("yyyy-MM-dd hh:mm:ss", dateReceived);
				}else{
					dateReceived = null;
				}
				
				if(dateCollected != null && dateCollected != "NA" && dateCollected != "Not provided"  
					&& dateCollected != "---" && dateCollected != "N/D" && dateCollected != "Date is not provided"
					&& dateCollected != "--"){
					//logger.info("Collected date: " + dateCollected);
					dateCollected = dateCollected + "";
					dateCollected = DateUtil.getDate("MM/dd/yyyy", dateCollected.substring(0, Math.min(dateCollected.length, 10)));
					dateCollected = DateUtil.formatDate("yyyy-MM-dd hh:mm:ss", dateCollected);
				}else{
					dateCollected = null;
				}


				//logger.info("Reported date: " + dateReported);
				dateReported = DateUtil.getDate("MM/dd/yyyy", dateReported.substring(0, Math.min(dateReported.length, 10)));
				dateReported = DateUtil.formatDate("yyyy-MM-dd hh:mm:ss", dateReported);

				//=== Specimen Info ===

				var targetSpecimenType = "Unknown";
				var srcSampleType = result.getString('Sample_Type');
				if(srcSampleType != null){
					srcSampleType = srcSampleType.toLowerCase();
					if(srcSampleType.indexOf("peripheral blood") > -1){
						targetSpecimenType = "Peripheral Blood";

					}else if(srcSampleType.indexOf("bone marrow") > -1){
						targetSpecimenType = "Bone Marrow";
						
					}else if(srcSampleType.indexOf("block") > -1){
						targetSpecimenType = "Block";
						
					}else if(srcSampleType.indexOf("tissue") > -1){
						targetSpecimenType = "Tissue";
						
					}else if(srcSampleType.indexOf("dna") > -1){
						targetSpecimenType = "DNA";
						
					}else if(srcSampleType.indexOf("lymphnode") > -1 || srcSampleType.indexOf("lymph node") > -1){
						targetSpecimenType = "Lymphnode";
						
					}else if(srcSampleType.indexOf("csf") > -1){
						targetSpecimenType = "CSF";
						
					}else if(srcSampleType.indexOf("urine") > -1){
						targetSpecimenType = "Urine";
						
					}else if(srcSampleType.indexOf("fluid") > -1){
						targetSpecimenType = "Fluid";
						
					}else if(srcSampleType.indexOf("fna") > -1){
						targetSpecimenType = "FNA";
						
					}else if(srcSampleType.indexOf("mass") > -1){
						targetSpecimenType = "Mass ( hip, breast, neck, axillary)";
						
					}else if(srcSampleType.indexOf("smears") > -1){
						targetSpecimenType = "Smears";
						
					}else if(srcSampleType.indexOf("slides") > -1){
						targetSpecimenType = "Unstained Slide";
						
					}else if(srcSampleType.indexOf("Biopsy") > -1){
						targetSpecimenType = "Biopsy";
						
					}
				}

				var clinicalHistory = result.getString('Clinical_History');
				//logger.info("Clinical History: " + clinicalHistory)

				//=== Case info ===
				var caseNumberVar = caseCode.split("-")[1];
				var insertCase = "INSERT INTO `CASE`(`CASE_CODE`,"
							+"`CASE_CATEGORY_ID`,"
							+"`CLIENT_ID`,"
							+"`PHYSICIAN_ID`,"
							+"`PATIENT_ID`,"
							+"`CASE_STATUS_ID`,"
							+"`CASE_PRIORITY_ID`,"
							+"`CASE_CREATION_DATE`,"
							+"`MISSING_INFORMATION`,"
							+"`CASE_COLLECTED_DATE`,"
							+"`CASE_RECEIVED_DATE`,"
							+"`CASE_SENT_DATE`,"
							+"`LEVEL_OF_SERVICE_ID`,"
							+"`CASE_NUMBER`,"
							+"`ML360_SPECIMENT_SOURCE`,"
							+"`CASE_CREATED_BY`,"
							+"`CASE_INDICATION`, "
							+"`CASE_CLINICAL_HISTORY`, "
							+"`CREATED_BY`,"
							+"`CREATION_DATE`,"
							+"`INSURANCE_ID`)"
							+"VALUES "
							+"('" + caseCode + "', " + caseCategoryId + ", " + clientId + ", " + physicianId + ", " + patientId + 
							", 6, 3, '" + dateReceived + "',0, " + 
							(dateCollected != null ? "'" + dateCollected + "'" : null)  + ", " + 
							(dateReceived != null ? "'" + dateReceived + "'" : null) + ", " +
							"'" + dateReported + "', 3, '" + 
							caseNumberVar + "', '" + targetSpecimenType + "', 'System', " + 
							(clinicalHistory != null? "'" + clinicalHistory + "'" : null) + ", " + 
							(clinicalHistory != null? "'" + clinicalHistory + "'" : null) + ", " +
							"'System', NOW(),  1)";
								
				tigerDbConnDest.executeUpdate(insertCase);

				var caseId;
				var lastReportGenerationIdVar;
				
				//logger.info("Selected case : " + caseCode);
				var caseIdResult = tigerDbConnDest.executeCachedQuery
				("SELECT CASE_ID, LAST_REPORT_GENERATION_ID FROM `CASE` WHERE `CASE_CODE` ='" + caseCode + "'");
			
				while(caseIdResult.next()){
					caseId = caseIdResult.getInt(1);
					lastReportGenerationIdVar = caseIdResult.getInt(2);
				}
				//logger.info("Case inserted successfully: " + caseId);
				 
				//=== Report generation ===
				if(lastReportGenerationIdVar == 0 ){
					
					var insertReportGeneration = "INSERT INTO `REPORT_GENERATION`"
									+"(`REPORT_GENERATION_SERIAL`,"
									+"`REPORT_CREATION_DATE`,"
									+"`REPORT_CREATED_BY`,"
									+"`REPORT_WORD_FILE_NAME`,"
									+"`REPORT_PDF_FILE_NAME`,"
									+"`REPORT_GENERATION_TEMPLATE_NAME`,"
									+"`CASE_ID`,"
									+"`REPORT_VERSION`,"
									+"`REPORT_GENERATED_OPERATION`,"
									+"`CREATED_BY`,"
									+"`CREATION_DATE`) "
									+"VALUES "
									+"(1, NOW(), 'System', '" + caseCode + ".docx', '" + caseCode + ".pdf', "
									+"'Siparadigm', " + caseId + ", 7, 'System', 'System', NOW())";
						
					tigerDbConnDest.executeUpdate(insertReportGeneration);
					var reportGenCriteriaInsert = tigerDbConnDest.executeUpdate("INSERT INTO REPORT_GENERATION_CRITERIA " + 
						"(REPORT_FORMAT_ID, REPORT_TYPE_ID, REPORT_TEMPLATE_ID, CREATED_BY, CREATION_DATE) " +
						"VALUES (1, 2, 1, 'System', NOW())");
					
					tigerDbConnDest.executeUpdate("UPDATE `CASE` " + 
						"SET LAST_REPORT_GENERATION_ID = (SELECT MAX(REPORT_GENERATION_ID) " +
							"FROM REPORT_GENERATION WHERE CASE_ID =" + caseId + "), " + 
						"CASE_REPORT_GENERATION_CRITERIA = (SELECT MAX(REPORT_GENERATION_CRITERIA_ID) " + 
							"FROM REPORT_GENERATION_CRITERIA) " +
						"WHERE CASE_ID = " + caseId);
				}
					rowsInserted++;
					logger.info("Inserted row number: " + rowsInserted);

			}catch(err){
				errorCounter++;
				logger.error(errorCounter + "- Error Occurred.. " + err.message);
				if(errorCounter >= 1){
					logger.error("Max number of errors is reached, aborting!, Case code: " + npId + ", records inserted: " + rowsInserted);
					throw "Max number of issues is reached.. aborting";
				}
			}
		}
		logger.info("Full data migration is succesfully done! # of migrated entries: " + rowsInserted);
	}catch(err){
		errorCounter++;
		logger.error(errorCounter + "- Error Occurred.. " + err.message);
		if(errorCounter >= 1){
			logger.error("Max number of errors is reached, aborting!, Case code: " + npId + ", records inserted: " + rowsInserted);
			throw "Max number of issues is reached.. aborting";
		}
	}finally {
		if (tigerDbConnSource) { 
			tigerDbConnSource.close();
		}
		if (tigerDbConnDest) { 
			tigerDbConnDest.close();
		}
	}
