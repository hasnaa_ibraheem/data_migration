CREATE SCHEMA `MIGRATE_CGI_DATA`;

CREATE TABLE MIGRATE_CGI_DATA.`novopath_from_reports` (
  `novo_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `Patient_First_Name` varchar(40) DEFAULT NULL,
  `Patient_Last_Name` varchar(30) DEFAULT NULL,
  `Patient_Date_of_Birth` varchar(40) DEFAULT NULL,
  `Physician_Name` varchar(80) DEFAULT NULL,
  `Practice_Name` varchar(120) DEFAULT NULL,
  `Practice_Address` varchar(120) DEFAULT NULL,
  `Practice_City` varchar(30) DEFAULT NULL,
  `Practice_State` varchar(20) DEFAULT NULL,
  `Practice_Zip` varchar(20) DEFAULT NULL,
  `Practice_Phone` varchar(20) DEFAULT NULL,
  `Case_Number` varchar(20) DEFAULT NULL,
  `Date_Received` varchar(40) DEFAULT NULL,
  `Date_Collected` varchar(40) DEFAULT NULL,
  `Date_Reported` varchar(40) DEFAULT NULL,
  `Sample_Type` varchar(140) DEFAULT NULL,
  `Clinical_History` varchar(670) DEFAULT NULL,
  `Report_Name` varchar(20) NOT NULL,
  PRIMARY KEY (`novo_table_id`),
  UNIQUE KEY `Case_Number` (`Case_Number`)
);

ALTER TABLE TIGER.`CASE` MODIFY COLUMN CASE_CLINICAL_HISTORY TEXT;